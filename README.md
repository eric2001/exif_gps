# EXIF_GPS
![EXIF_GPS](./screenshot.jpg) 

## Description
The EXIF_GPS module is a Gallery 3 module which will extract Latitude and Longitude information from a photos exif data. The data will be automatically extracted from photos as they're uploaded, if it's present. Additionally, a maintenance task is available to scan the entire Gallery 3 library for photos that were uploaded before this module is active, in order to extract GPS data from older pictures. This module also allows users to manually assign Latitude and Longitude values by using the edit menu option (Edit photo, Edit album, etc). The stored coordinates are then used to display a small google map in the sidebar, showing where the photo was taken. Also, for anyone using my TagsMap module, the sidebar is TagsMap aware -- in the event that the current item does not have exif coordinates associated with it, but does have a gps-tagged tag assigned to it, the coordinates from the tag will be used instead.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "exif_gps" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

## History
**Version 3.1.1:**
> - Includes bozek's fix for excessive database queries ( http://gallery.menalto.com/node/109227#comment-399841 ).
> - Released 11 December 2012.
>
> Download: [Version 3.1.1](/exif_gps/uploads/5f44eadc7b948361a9b32564767d5b8a/exif_gps311.zip)

**Version 3.1.0:**
> - Re-wrote code to load gps coordinates 1000 at a time instead of all at once.
> - Changed "Loading..." message to "Loading... %" to show how far along it is.
> - Released 27 June 2012.
>
> Download: [Version 3.1.0](/uploads/ce92ccacd7666f760950e42148f593fd/exif_gps310.zip)

**Version 3.0.1:**
> - Two minor bug fixes.
> - Tested with Gallery 3.0.4.
> - Released 20 June 2012.
>
> Download: [Version 3.0.1](/uploads/da981a6bff98b0cc2f2e9f93577d9ceb/exif_gps301.zip)

**Version 3.0.0:**
> - Changed references to Google Maps API key to Google APIs Console key.
> - Made API keys optional.
> - Added option to restrict viewing maps to registered users (off by default).
> - Added a map to user profile pages.
> - Added breadcrumbs to map pages.
> - Completely re-written javascript code -- gps coordinates are now loaded from a separate file after the page loads.
> - When zoomed out, gps coordinates that are close together will be grouped together.
> - Released 18 June 2012.
>
> Download: [Version 3.0.0](/uploads/4c7db9dc2a5c760c393a4d5e36fa1dcc/exif_gps300.zip)

**Version 2.1.4:**
> - Now displays a warning message that the EXIF module is required when activating it.
> - Released 15 May 2011.
>
> Download: [Version 2.1.4](/uploads/b619bf8b52e1aef08f8b02d385fcc473/exif_gps214.zip)

**Version 2.1.3:**
> - Implemented mattbostock suggestion for gracefully degrading the sidebar map for browsers that don't support javascript.
> - Implemented niaxilin's auto-zoom bugfix.
> - Released 25 April 2011.
>
> Download: [Version 2.1.3](/uploads/ed52edf6754ca156999edef9d217d79f/exif_gps213.zip)

**Version 2.1.2:**
> - Minor Bugfixes.
> - Tested for use with Gallery 3.0.1.
> - Released 2 February 2011.
>
> Download: [Version 2.1.2](/uploads/8612db54daa95ce8147f6e728c030530/exif_gps212.zip)

**Version 2.1.1:**
> - Bug Fix:  Calling the is_album() function from a dynamic page causes the page to crash. This is now fixed.
> - Released 07 April 2010.
>
> Download: [Version 2.1.1](/uploads/5280d0291e07bcf315d2a3156e748020/exif_gps211.zip)

**Version 2.1.0:**
> - Added optional toolbar icons for accessing maps of the current album/user.
> - Changed map labels on the admin screen to match the labels that are displayed on the actual map.
> - Added code to map the contents of the album in the sidebar if there are not coordinates available for the actual album.
> - Added a maximum auto-zoom level to provide a way for admins to prevent the module from selecting a starting zoom that doesn't have satellite imagery available. 
> - Released 01 April 2010.
>
> Download: [Version 2.1.0](/uploads/d604d93edf9f56a06c82b3735eaccdf0/exif_gps210.zip)

**Version 2.0.1:**
> - Map this album/Map this user links are now translatable.
> - Added some additional text to the admin screen to explain where to get a Google API key from and how the Zoom Level value works.
> - Added a return to album link on the "Map this album" screen.
> - Released 18 March 2010
>
> Download: [Version 2.0.1](/uploads/2301c32ad6243dc9bc3d55312491d6ae/exif_gps201.zip)

**Version 2.0.0:**
> - Fixed sddroog's issue with GPS coordinates not extracting properly.
> - Added in Serge D's suggestion for putting the Edit Photo->Latitude and Longitude fields on one line.
> - Added an admin screen.
> - Made the sidebar customizable: Google map controls can be turned on/off, map type can be changed, default zoom level can be changed.
> - Sidebar will now show up on tag pages, if tagsmap is enabled and if the tag has coordinates.
> - Added "Map this album" and "Map this user" sidebar links.
> - Released 17 March 2010.
>
> Download: [Version 2.0.0](/uploads/41812a5b1a7e6c72986ffd97f58d717c/exif_gps200.zip)

**Version 1.0.1:**
> - Bug Fix:  Make sure coordinates are stored with a decimal and not a comma (issue with certain foreign language translations).
> - Released 10 March 2010.
>
> Download: [Version 1.0.1](/uploads/b0354975e8765c4038effc257c4fd763/exif_gps101.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 04 March 2010.
>
> Download: [Version 1.0.0](/uploads/1ce0c422d2b196998c4ee8cdec71ef27/exif_gps100.zip)
